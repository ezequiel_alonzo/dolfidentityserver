﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using IdentityServer.DbContexts;
using IdentityServer.Helpers;
using IdentityServer.Models;
using Newtonsoft.Json;

namespace IdentityServer.Controllers
{
    public class ClientController : ApiController
    {
        /// <summary>
        /// Returns client details for provided client id.
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("getclientdetails")]
        public async Task<string> GetClientDetails(string clientId)
        {
            try
            {
                if (Utils.IsNull(clientId)) return null;

                var db = new IsContext();

                var cName = clientId.Trim().ToLower();

                var clientProf = await db.ClientProfiles.FirstOrDefaultAsync(a => a.ClientId == cName);

                if (clientProf == null) return null;

                var sItem = Utils.ClientProfileToClientModel(clientProf);

                return sItem == null ? null : JsonConvert.SerializeObject(sItem);
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Returns a list of clients.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ActionName("getclientlist")]
        public async Task<string> GetClientList()
        {
            try
            {
                var db = new IsContext();

                var cList = await db.ClientProfiles.OrderBy(a => a.ClientId).ToListAsync();

                if (cList == null) return null;

                var nList = cList.Select(Utils.ClientProfileToClientModel).Where(tmpClient => tmpClient != null).ToList();

                return JsonConvert.SerializeObject(nList);
            }
            catch
            {
                return null;
            }
        }

        [HttpGet]
        [ActionName("getclientnames")]
        public async Task<string> GetClientNames()
        {
            try
            {
                var db = new IsContext();

                var cList = await db.ClientProfiles.OrderBy(a => a.ClientId).ToListAsync();

                if (cList == null) return null;

                var nList = cList.Select(a => a.ClientId).Where(tmpClient => tmpClient != null).ToList();

                return JsonConvert.SerializeObject(nList);
            }
            catch
            {
                return null;
            }
        }

        [HttpPost]
        [ActionName("addclient")]
        public IHttpActionResult AddClient(ClientModel model)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest("Invalid Model");
                if (Utils.IsNull(model.ClientId)) return BadRequest("Missing Client Id");

                var db = new IsContext();

                var cName = model.ClientId.Trim().ToLower();

                var scopeChk = db.ClientProfiles.FirstOrDefault(a => a.ClientId == cName);

                //Client Name exists
                if (scopeChk != null) return BadRequest("Client Id Exists");

                var nModel = Utils.ClientModelToClientProfile(model);

                if (nModel == null) return BadRequest("Invalid Model");

                db.ClientProfiles.Add(nModel);
                db.SaveChanges();

                return Ok();
            }
            catch
            {
                return BadRequest("Internal Server Error");
            }
        }

        [HttpPost]
        [ActionName("updateclient")]
        public IHttpActionResult UpdateClient(ClientModel model)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest("Invalid Model");
                if (Utils.IsNull(model.ClientId)) return BadRequest("Missing Client Id");

                var db = new IsContext();

                var cName = model.ClientId.Trim().ToLower();

                var cItem = db.ClientProfiles.FirstOrDefault(a => a.ClientId == cName);

                //Scope Name does not exist
                if (cItem == null) return BadRequest("Unable To Locate Client Id");

                cItem.ClientName = model.ClientName;
                cItem.ClientUri = model.ClientUri;
                cItem.Flow = model.Flow;
                cItem.LogoUri = model.LogoUri;
                cItem.LogoutUri = model.LogoutUri;
                cItem.RefreshTokenExpiration = model.RefreshTokenExpiration;
                cItem.RefreshTokenUsage = model.RefreshTokenUsage;
                cItem.AccessTokenType = model.AccessTokenType;
                cItem.SlidingRefreshTokenLifetime = model.SlidingRefreshTokenLifetime;
                cItem.AbsoluteRefreshTokenLifetime = model.AbsoluteRefreshTokenLifetime;
                cItem.AccessTokenLifetime = model.AccessTokenLifetime;
                cItem.IdentityTokenLifetime = model.IdentityTokenLifetime;
                cItem.AuthorizationCodeLifetime = model.AuthorizationCodeLifetime;
                cItem.RequireConsent = model.RequireConsent;
                cItem.LogoutSessionRequired = model.LogoutSessionRequired;
                cItem.EnableLocalLogin = model.EnableLocalLogin;
                cItem.AllowRememberConsent = model.AllowRememberConsent;
                cItem.AlwaysSendClientClaims = model.AlwaysSendClientClaims;
                cItem.AllowAccessToAllCustomGrantTypes = model.AllowAccessToAllCustomGrantTypes;
                cItem.AllowAccessToAllScopes = model.AllowAccessToAllScopes;
                cItem.AllowClientCredentialsOnly = model.AllowClientCredentialsOnly;
                cItem.PrefixClientClaims = model.PrefixClientClaims;
                cItem.IncludeJwtId = model.IncludeJwtId;
                cItem.UpdateAccessTokenClaimsOnRefresh = model.UpdateAccessTokenClaimsOnRefresh;
                cItem.Claims = Utils.ClaimListToString(model.Claims);
                cItem.ClientSecrets = Utils.SecretListToString(model.ClientSecrets);
                cItem.IdentityProviderRestrictions = Utils.StringListToString(model.IdentityProviderRestrictions);
                cItem.PostLogoutRedirectUris = Utils.StringListToString(model.PostLogoutRedirectUris);
                cItem.RedirectUris = Utils.StringListToString(model.RedirectUris);
                cItem.AllowedCorsOrigins = Utils.StringListToString(model.AllowedCorsOrigins);
                cItem.AllowedCustomGrantTypes = Utils.StringListToString(model.AllowedCustomGrantTypes);
                cItem.AllowedScopes = Utils.StringListToString(model.AllowedScopes);

                db.SaveChanges();

                return Ok();
            }
            catch
            {
                return BadRequest("Internal Server Error");
            }
        }

        [HttpPost]
        [ActionName("enableclient")]
        public IHttpActionResult EnableClient(ClientModel model)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest("Invalid Model");
                if (Utils.IsNull(model.ClientId)) return BadRequest("Missing Client Id");

                var db = new IsContext();

                var cName = model.ClientId.Trim().ToLower();

                var clientProf = db.ClientProfiles.FirstOrDefault(a => a.ClientId == cName);

                if (clientProf == null) return BadRequest("Unable To Locate Client Id");

                if (clientProf.Enabled) return Ok();

                clientProf.Enabled = true;

                db.SaveChanges();

                return Ok();
            }
            catch
            {
                return BadRequest("Internal Server Error");
            }
        }

        [HttpPost]
        [ActionName("disableclient")]
        public IHttpActionResult DisableClient(ClientModel model)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest("Invalid Model");
                if (Utils.IsNull(model.ClientId)) return BadRequest("Missing Client Id");

                var db = new IsContext();

                var cName = model.ClientId.Trim().ToLower();

                var clientProf = db.ClientProfiles.FirstOrDefault(a => a.ClientId == cName);

                if (clientProf == null) return BadRequest("Unable To Locate Client Id");

                if (!clientProf.Enabled) return Ok();

                clientProf.Enabled = false;

                db.SaveChanges();

                return Ok();
            }
            catch
            {
                return BadRequest("Internal Server Error");
            }
        }

        [HttpPost]
        [ActionName("deleteclient")]
        public IHttpActionResult DeleteClient(ClientModel model)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest("Invalid Model");
                if (Utils.IsNull(model.ClientId)) return BadRequest("Missing Client Id");

                var db = new IsContext();

                var cName = model.ClientId.Trim().ToLower();

                var clientProf = db.ClientProfiles.FirstOrDefault(a => a.ClientId == cName);

                if (clientProf == null) return BadRequest("Unable To Locate Client Id");

                db.ClientProfiles.Remove(clientProf);
                db.SaveChanges();

                return Ok();
            }
            catch
            {
                return BadRequest("Internal Server Error");
            }
        }

    }
}
