﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using IdentityModel.Client;
using IdentityServer.DbContexts;
using IdentityServer.Helpers;
using IdentityServer.Infrastructure;
using IdentityServer.Models;
using IdentityServer3.Core.Models;
using IdentityServer3.Core.Services.Default;
using Newtonsoft.Json;
using TokenResponse = IdentityModel.Client.TokenResponse;

namespace IdentityServer.Controllers
{
    public class UserController : ApiController
    {
        private readonly LocalUserService _client = new LocalUserService(new UserServiceBase());

        /// <summary>
        /// Local user login.
        /// </summary>
        /// <param name="model">Username,Password, and (ClientId, Scope, or ClientId & Scope) are required.</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ActionName("userlogin")]
        public async Task<string> UserLogin([FromBody] UserLoginModel model)
        {
            try
            {
                var isValidUser = _client.ValidateLocalUser(model);

                if (isValidUser.IsError) return isValidUser.ErrorMessage;

                var user = new LocalAuthenticationContext()
                {
                    UserName = model.UserName,
                    Password = model.Password
                };

                await _client.AuthenticateLocalAsync(user);

                var claims = user.AuthenticateResult.User.Claims.ToList();

                if (!claims.Any()) return NotFound().ToString();

                var uClaims = Utils.ClaimsToClaimModel(claims.ToList());

                var resp = new LocalUserModel()
                {
                    IsEnabled = true,
                    UserName = model.UserName,
                    Claims = uClaims
                };

                return JsonConvert.SerializeObject(resp);
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Local user logout.
        /// </summary>
        /// <param name="model">Model returned by UserLogin is required for logout.</param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("userlogout")]
        public async Task<IHttpActionResult> UserLogout(LocalUserModel model)
        {
            try
            {
                var tmpclaims = Utils.ClaimModelToString(model.Claims);
                var claims = Utils.StringToClaims(tmpclaims);

                var context = new SignOutContext()
                {
                    Subject = new ClaimsPrincipal(new ClaimsIdentity(claims))
                };

                await _client.SignOutAsync(context);

                return Ok();
            }
            catch
            {
                return BadRequest("Internal Server Error");
            }
        }

        /// <summary>
        /// Returns a TokenResponse for the ClientId and Secret provided.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ActionName("clientlogin")]
        public async Task<string> ClientLogin([FromBody] ClientLoginModel model)
        {
            try
            {
                const string clientLoginUrl = "/identity/connect/token";

                var urlPrefix = Request.RequestUri.AbsoluteUri.Replace(Request.RequestUri.AbsolutePath, "");

                var client = new TokenClient(urlPrefix + clientLoginUrl, model.ClientId, model.Secret);

                var token = await client.RequestClientCredentialsAsync(model.Scope);

                return JsonConvert.SerializeObject(token);
            }
            catch
            {
                return null;
            }
        }

        [HttpPost]
        [ActionName("clientlogout")]
        public async Task<IHttpActionResult> ClientLogout(TokenResponse model)
        {
            

            return Ok();
        }

        /// <summary>
        /// Returns user details.
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("getuserdetails")]
        public async Task<string> GetUserDetails(string userName)
        {
            
            try
            {
                if (Utils.IsNull(userName)) return null;

                var db = new IsContext();

                var uName = userName.Trim().ToLower();

                var userProf = await db.UserProfiles.FirstOrDefaultAsync(a => a.Username == uName);

                if (userProf == null) return null;

                var tmpClaims = Utils.StringToClaims(userProf.Claims).ToList();
                var claims = Utils.ClaimsToClaimModel(tmpClaims);

                var response = new LocalUserModel()
                {
                    UserName = userProf.Username,
                    IsEnabled = userProf.Enabled,
                    Claims = claims
                };

                return JsonConvert.SerializeObject(response);
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Returns a list of users with optional filters (All, Role, ClientId, Scope).
        /// </summary>
        /// <param name="userRole"></param>
        /// <param name="userClientId"></param>
        /// <param name="userScope"></param>
        [HttpGet]
        [ActionName("getuserlist")]
        public async Task<string> GetUserList(string userRole = "", string userClientId = "", string userScope = "")
        {
            try
            {
                var db = new IsContext();

                var isUserRoleNull = Utils.IsNull(userRole);
                var isUserClientIdNull = Utils.IsNull(userClientId);
                var isUserScopeNull = Utils.IsNull(userScope);

                var uList = await db.UserProfiles.Select(a => a).ToListAsync();

                var userList = (from user in uList
                    let tmpClaims = Utils.StringToClaims(user.Claims).ToList()
                    let claims = Utils.ClaimsToClaimModel(tmpClaims)
                    let uItem = new LocalUserModel()
                    {
                        IsEnabled = user.Enabled, UserName = user.Username, Claims = claims
                    }
                    where HasClaimFilter(claims, isUserRoleNull, userRole, isUserClientIdNull, userClientId, isUserScopeNull, userScope)
                    select uItem).ToList();

                return JsonConvert.SerializeObject(userList);
            }
            catch
            {
                return null;
            }
        }
        private static bool HasClaimFilter(ClaimModel claims, bool isUserRoleNull, string userRole, bool isUserClientIdNull, string userClientId, bool isUserScopeNull, string userScope)
        {
            if (isUserRoleNull && isUserClientIdNull && isUserScopeNull)
            {
                return true;
            }

            if (!isUserScopeNull && isUserRoleNull && isUserClientIdNull)
            {
                return claims.Scopes.Contains(userScope);
            }

            if (!isUserClientIdNull && isUserRoleNull && isUserScopeNull)
            {
                return claims.Clients.Contains(userClientId);
            }

            if (!isUserRoleNull && isUserClientIdNull && isUserScopeNull)
            {
                return claims.Roles.Contains(userRole);
            }

            if (!isUserRoleNull && !isUserScopeNull && isUserClientIdNull)
            {
                return claims.Roles.Contains(userRole) && claims.Scopes.Contains(userScope);
            }

            if (!isUserRoleNull && !isUserClientIdNull && isUserScopeNull)
            {
                return claims.Roles.Contains(userRole) && claims.Clients.Contains(userClientId);
            }

            if (!isUserClientIdNull && !isUserScopeNull && isUserRoleNull)
            {
                return claims.Clients.Contains(userClientId) && claims.Scopes.Contains(userScope);
            }

            if (!isUserRoleNull && !isUserClientIdNull && !isUserScopeNull)
            {
                return claims.Roles.Contains(userRole) && claims.Clients.Contains(userClientId) &&
                       claims.Scopes.Contains(userScope);
            }          

            return false;
        }

        /// <summary>
        /// Adds a user.  If user exists, the user details will be updated with a subset of the provided details, 
        /// but Enabled and Password properties will not be updated.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("adduser")]
        public IHttpActionResult AddUser([FromBody] LocalUserModel model)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest("Invalid Model");
                if (Utils.IsNull(model.UserName)) return BadRequest("Missing UserName");
                if(model.Claims == null) model.Claims = new ClaimModel();


                if (Utils.IsNull(model.Claims.Email)) model.Claims.Email = model.UserName;

                var db = new IsContext();

                var uName = model.UserName.Trim().ToLower();
                var uPwd = Utils.Encrypt(model.Password.Trim());

                var userChk = db.UserProfiles.FirstOrDefault(a => a.Username == uName);

                //Username exists - Update with new details
                if (userChk != null)
                {
                    var currClaims = Utils.StringToClaimModel(userChk.Claims);

                    var newClaims = Utils.GetClaimsSubset(currClaims, model.Claims);

                    userChk.Claims = Utils.ClaimModelToString(newClaims);
                    db.SaveChanges();

                    return Ok();
                }

                if (model.Claims.Clients == null || !model.Claims.Clients.Any()) return BadRequest("Missing Client");
                if (Utils.IsNull(model.Password)) return BadRequest("Missing Password");

                var user = new UserProfile
                {
                    Enabled = true,
                    Username = uName,
                    Password = uPwd,
                    Subject = Guid.NewGuid().ToString(),
                    Claims = Utils.ClaimModelToString(model.Claims),
                };

                db.UserProfiles.Add(user);
                db.SaveChanges();

                return Ok();
            }
            catch
            {
                return BadRequest("Internal Server Error");
            }

        }

        /// <summary>
        /// Updates user demographic details. Access properties are not updated.
        /// </summary>
        /// <param name="model">UserName required.</param>
        [HttpPost]
        [ActionName("updateuser")]
        public IHttpActionResult UpdateUser([FromBody] LocalUserModel model)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest("Invalid Model");
                if (Utils.IsNull(model.UserName)) return BadRequest("Missing UserName");

                var db = new IsContext();

                var uName = model.UserName.Trim().ToLower();

                var userProf = db.UserProfiles.FirstOrDefault(a => a.Username == uName);

                if (userProf == null) return BadRequest("Unable To Locate User");
                
                var currClaims = Utils.StringToClaimModel(userProf.Claims);
                var nClaims = model.Claims;

                var newClaims = new ClaimModel()
                {
                    Address = nClaims.Address,
                    BirthDate = nClaims.BirthDate,
                    FirstName = nClaims.FirstName,
                    MiddleName = nClaims.MiddleName,
                    LastName = nClaims.LastName,
                    FullName = nClaims.FullName,
                    NickName = nClaims.NickName,
                    Gender = nClaims.Gender,
                    PhoneNumber = nClaims.PhoneNumber,
                    PhotoUrl = nClaims.PhotoUrl,
                    Email = nClaims.Email,
                    EmailVerified = nClaims.EmailVerified,
                    Expiration = nClaims.Expiration,
                    ExternalProviderUserIds = nClaims.ExternalProviderUserIds,
                    Roles = nClaims.Roles,
                    Scopes = nClaims.Scopes,
                    Clients = nClaims.Clients
                };

                userProf.Claims = Utils.ClaimModelToString(newClaims);

                db.SaveChanges();

                return Ok();
            }
            catch
            {
                return BadRequest("Internal Server Error");
            }
        }

        /// <summary>
        /// Enables user.
        /// </summary>
        /// <param name="model">UserName required.</param>
        [HttpPost]
        [ActionName("enableuser")]
        public IHttpActionResult EnableUser([FromBody] LocalUserModel model)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest("Invalid Model");
                if (Utils.IsNull(model.UserName)) return BadRequest("Missing UserName");

                var db = new IsContext();

                var userProf = db.UserProfiles.FirstOrDefault(a => a.Username == model.UserName);

                if(userProf == null) return BadRequest("Unable To Locate User");

                if (userProf.Enabled) return Ok();

                userProf.Enabled = true;

                db.SaveChanges();

                return Ok();
            }
            catch
            {
                return BadRequest("Internal Server Error");
            }
        }

        /// <summary>
        /// Disable user.
        /// </summary>
        /// <param name="model">UserName required.</param>
        [HttpPost]
        [ActionName("disableuser")]
        public IHttpActionResult DisableUser([FromBody] LocalUserModel model)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest("Invalid Model");
                if (Utils.IsNull(model.UserName)) return BadRequest("Missing UserName");

                var db = new IsContext();

                var userProf = db.UserProfiles.FirstOrDefault(a => a.Username == model.UserName);

                if (userProf == null) return BadRequest("Unable To Locate User");

                if (!userProf.Enabled) return Ok();

                userProf.Enabled = false;

                db.SaveChanges();

                return Ok();
            }
            catch
            {
                return BadRequest("Internal Server Error");
            }
        }

        /// <summary>
        /// Sets user expiration date to disable user.
        /// </summary>
        /// <param name="model">UserName and Claims.Expiration required.</param>
        [HttpPost]
        [ActionName("setuserexpiration")]
        public IHttpActionResult SetUserExpiration([FromBody] LocalUserModel model)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest("Invalid Model");
                if (Utils.IsNull(model.UserName)) return BadRequest("Missing UserName");

                var db = new IsContext();

                var userProf = db.UserProfiles.FirstOrDefault(a => a.Username == model.UserName);

                if (userProf == null) return BadRequest("Unable To Locate User");

                var claims = Utils.StringToClaimModel(userProf.Claims);

                claims.Expiration = model.Claims.Expiration;

                userProf.Claims = Utils.ClaimModelToString(claims);

                db.SaveChanges();

                return Ok();
            }
            catch
            {
                return BadRequest("Internal Server Error");
            }
        }

        /// <summary>
        /// Deletes user.
        /// </summary>
        /// <param name="model">UserName required.</param>
        [HttpPost]
        [ActionName("deleteuser")]
        public IHttpActionResult DeleteUser([FromBody] LocalUserModel model)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest("Invalid Model");
                if (Utils.IsNull(model.UserName)) return BadRequest("Missing UserName");

                var db = new IsContext();

                var userProf = db.UserProfiles.FirstOrDefault(a => a.Username == model.UserName);

                if (userProf == null) return BadRequest("Unable To Locate User");

                db.UserProfiles.Remove(userProf);

                db.SaveChanges();

                return Ok();
            }
            catch
            {
                return BadRequest("Internal Server Error");
            }
        }

        /// <summary>
        /// Adds clients to user.
        /// </summary>
        /// <param name="model">UserName and Claims.Clients are required.</param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("adduserclients")]
        public IHttpActionResult AddUserClients([FromBody] LocalUserModel model)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest("Invalid Model");
                if (Utils.IsNull(model.UserName)) return BadRequest("Missing UserName");
                if (model.Claims == null) return BadRequest("Missing Clients To Add");
                if (model.Claims.Clients == null) return BadRequest("Missing Clients To Add");
                if (!model.Claims.Clients.Any()) return BadRequest("Missing Clients To Add");

                var db = new IsContext();

                var userProf = db.UserProfiles.FirstOrDefault(a => a.Username == model.UserName);

                if (userProf == null) return BadRequest("Unable To Locate User");

                var claims = Utils.StringToClaimModel(userProf.Claims);
                var nClaims = model.Claims;

                foreach (var client in nClaims.Clients.Where(client => !claims.Clients.Contains(client)))
                {
                    claims.Clients.Add(client);
                }

                userProf.Claims = Utils.ClaimModelToString(claims);

                db.SaveChanges();

                return Ok();
            }
            catch
            {
                return BadRequest("Internal Server Error");
            }
        }

        /// <summary>
        /// Removes clients from user.
        /// </summary>
        /// <param name="model">UserName and Claims.Clients are required.</param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("removeuserclients")]
        public IHttpActionResult RemoveUserClients([FromBody] LocalUserModel model)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest("Invalid Model");
                if (Utils.IsNull(model.UserName)) return BadRequest("Missing UserName");
                if (model.Claims == null) return BadRequest("Missing Clients To Remove");
                if (model.Claims.Clients == null) return BadRequest("Missing Clients To Remove");
                if (!model.Claims.Clients.Any()) return BadRequest("Missing Clients To Remove");

                var db = new IsContext();

                var userProf = db.UserProfiles.FirstOrDefault(a => a.Username == model.UserName);

                if (userProf == null) return BadRequest("Unable To Locate User");

                var claims = Utils.StringToClaimModel(userProf.Claims);
                var nClaims = model.Claims;

                foreach (var client in nClaims.Clients.Where(client => claims.Clients.Contains(client)))
                {
                    claims.Clients.Remove(client);
                }

                userProf.Claims = Utils.ClaimModelToString(claims);

                db.SaveChanges();

                return Ok();
            }
            catch
            {
                return BadRequest("Internal Server Error");
            }
        }

        /// <summary>
        /// Adds roles to user.
        /// </summary>
        /// <param name="model">UserName and Claims.Roles are required.</param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("adduserroles")]
        public IHttpActionResult AddUserRoles([FromBody] LocalUserModel model)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest("Invalid Model");
                if (Utils.IsNull(model.UserName)) return BadRequest("Missing UserName");
                if (model.Claims == null) return BadRequest("Missing Roles To Add");
                if (model.Claims.Roles == null) return BadRequest("Missing Roles To Add");
                if (!model.Claims.Roles.Any()) return BadRequest("Missing Roles To Add");

                var db = new IsContext();

                var userProf = db.UserProfiles.FirstOrDefault(a => a.Username == model.UserName);

                if (userProf == null) return BadRequest("Unable To Locate User");

                var claims = Utils.StringToClaimModel(userProf.Claims);
                var nClaims = model.Claims;

                foreach (var role in nClaims.Roles.Where(role => !claims.Roles.Contains(role)))
                {
                    claims.Roles.Add(role);
                }

                userProf.Claims = Utils.ClaimModelToString(claims);

                db.SaveChanges();

                return Ok();
            }
            catch
            {
                return BadRequest("Internal Server Error");
            }
        }

        /// <summary>
        /// Removes roles from user.
        /// </summary>
        /// <param name="model">UserName and Claims.Roles are required.</param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("removeuserroles")]
        public IHttpActionResult RemoveUserRoles([FromBody] LocalUserModel model)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest("Invalid Model");
                if (Utils.IsNull(model.UserName)) return BadRequest("Missing UserName");
                if (model.Claims == null) return BadRequest("Missing Roles To Remove");
                if (model.Claims.Roles == null) return BadRequest("Missing Roles To Remove");
                if (!model.Claims.Roles.Any()) return BadRequest("Missing Roles To Remove");

                var db = new IsContext();

                var userProf = db.UserProfiles.FirstOrDefault(a => a.Username == model.UserName);

                if (userProf == null) return BadRequest("Unable To Locate User");

                var claims = Utils.StringToClaimModel(userProf.Claims);
                var nClaims = model.Claims;

                foreach (var role in nClaims.Roles.Where(role => claims.Roles.Contains(role)))
                {
                    claims.Roles.Remove(role);
                }

                userProf.Claims = Utils.ClaimModelToString(claims);

                db.SaveChanges();

                return Ok();
            }
            catch
            {
                return BadRequest("Internal Server Error");
            }
        }

        /// <summary>
        /// Adds scopes to user.
        /// </summary>
        /// <param name="model">UserName and Claims.Scopes are required.</param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("adduserscopes")]
        public IHttpActionResult AddUserScopes([FromBody] LocalUserModel model)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest("Invalid Model");
                if (Utils.IsNull(model.UserName)) return BadRequest("Missing UserName");
                if (model.Claims == null) return BadRequest("Missing Scopes To Add");
                if (model.Claims.Scopes == null) return BadRequest("Missing Scopes To Add");
                if (!model.Claims.Scopes.Any()) return BadRequest("Missing Scopes To Add");

                var db = new IsContext();

                var userProf = db.UserProfiles.FirstOrDefault(a => a.Username == model.UserName);

                if (userProf == null) return BadRequest("Unable To Locate User");

                var claims = Utils.StringToClaimModel(userProf.Claims);
                var nClaims = model.Claims;

                foreach (var scope in nClaims.Scopes.Where(scope => !claims.Scopes.Contains(scope)))
                {
                    claims.Scopes.Add(scope);
                }

                userProf.Claims = Utils.ClaimModelToString(claims);

                db.SaveChanges();

                return Ok();
            }
            catch
            {
                return BadRequest("Internal Server Error");
            }
        }

        /// <summary>
        /// Removes scopes from user.
        /// </summary>
        /// <param name="model">UserName and Claims.Scopes are required.</param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("removeuserscopes")]
        public IHttpActionResult RemoveUserScopes([FromBody] LocalUserModel model)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest("Invalid Model");
                if (Utils.IsNull(model.UserName)) return BadRequest("Missing UserName");
                if (model.Claims == null) return BadRequest("Missing Scopes To Remove");
                if (model.Claims.Scopes == null) return BadRequest("Missing Scopes To Remove");
                if (!model.Claims.Scopes.Any()) return BadRequest("Missing Scopes To Remove");

                var db = new IsContext();

                var userProf = db.UserProfiles.FirstOrDefault(a => a.Username == model.UserName);

                if (userProf == null) return BadRequest("Unable To Locate User");

                var claims = Utils.StringToClaimModel(userProf.Claims);
                var nClaims = model.Claims;

                foreach (var scope in nClaims.Scopes.Where(scope => claims.Scopes.Contains(scope)))
                {
                    claims.Scopes.Remove(scope);
                }

                userProf.Claims = Utils.ClaimModelToString(claims);

                db.SaveChanges();

                return Ok();
            }
            catch
            {
                return BadRequest("Internal Server Error");
            }
        }

        /// <summary>
        /// Adds external user ids to user.
        /// </summary>
        /// <param name="model">UserName and Claims.ExternalProviderUserIds are required.</param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("adduserexternaluserids")]
        public IHttpActionResult AddUserExternalUserIds([FromBody] LocalUserModel model)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest("Invalid Model");
                if (Utils.IsNull(model.UserName)) return BadRequest("Missing UserName");
                if (model.Claims == null) return BadRequest("Missing ExternalProviderUserIds To Add");
                if (model.Claims.ExternalProviderUserIds == null) return BadRequest("Missing External User Ids To Add");
                if (!model.Claims.ExternalProviderUserIds.Any()) return BadRequest("Missing External User Ids To Add");

                var db = new IsContext();

                var userProf = db.UserProfiles.FirstOrDefault(a => a.Username == model.UserName);

                if (userProf == null) return BadRequest("Unable To Locate User");

                var claims = Utils.StringToClaimModel(userProf.Claims);
                var nClaims = model.Claims;

                foreach (var userId in nClaims.ExternalProviderUserIds.Where(userId => !claims.ExternalProviderUserIds.Contains(userId)))
                {
                    claims.ExternalProviderUserIds.Add(userId);
                }

                userProf.Claims = Utils.ClaimModelToString(claims);

                db.SaveChanges();

                return Ok();
            }
            catch
            {
                return BadRequest("Internal Server Error");
            }
        }

        /// <summary>
        /// Removes external user ids from user.
        /// </summary>
        /// <param name="model">UserName and Claims.ExternalProviderUserIds are required.</param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("removeuserexternaluserids")]
        public IHttpActionResult RemoveUserExternalUserIds([FromBody] LocalUserModel model)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest("Invalid Model");
                if (Utils.IsNull(model.UserName)) return BadRequest("Missing UserName");
                if (model.Claims == null) return BadRequest("Missing ExternalProviderUserIds To Remove");
                if (model.Claims.ExternalProviderUserIds == null) return BadRequest("Missing External User Ids To Remove");
                if (!model.Claims.ExternalProviderUserIds.Any()) return BadRequest("Missing External User Ids To Remove");

                var db = new IsContext();

                var userProf = db.UserProfiles.FirstOrDefault(a => a.Username == model.UserName);

                if (userProf == null) return BadRequest("Unable To Locate User");

                var claims = Utils.StringToClaimModel(userProf.Claims);
                var nClaims = model.Claims;

                foreach (var userId in nClaims.ExternalProviderUserIds.Where(userId => claims.ExternalProviderUserIds.Contains(userId)))
                {
                    claims.ExternalProviderUserIds.Remove(userId);
                }

                userProf.Claims = Utils.ClaimModelToString(claims);

                db.SaveChanges();

                return Ok();
            }
            catch
            {
                return BadRequest("Internal Server Error");
            }
        }

        /// <summary>
        /// Changes user password.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("changeuserpassword")]
        public IHttpActionResult ChangeUserPassword([FromBody] UserChangePasswordModel model)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest("Invalid Model");
                if (Utils.IsNull(model.UserName)) return BadRequest("Missing UserName");
                if (Utils.IsNull(model.OldPassword)) return BadRequest("Missing Old Password");
                if (Utils.IsNull(model.NewPassword)) return BadRequest("Missing New Password");
                if (Utils.IsNull(model.ConfirmNewPassword)) return BadRequest("Missing New Password Confirm");
                if (model.NewPassword != model.ConfirmNewPassword) return BadRequest("New Password and Confirm Password Do Not Match");

                var db = new IsContext();

                var userProf = db.UserProfiles.FirstOrDefault(a => a.Username == model.UserName);

                if (userProf == null) return BadRequest("Unable To Locate User");

                var currPwd = Utils.Decrypt(userProf.Password);
                var oldPwd = model.OldPassword;

                if(currPwd != oldPwd) return BadRequest("Old Password Incorrect");

                var newPwd = Utils.Encrypt(model.NewPassword);

                userProf.Password = newPwd;

                db.SaveChanges();

                return Ok();
            }
            catch
            {
                return BadRequest("Internal Server Error");
            }
        }

    }
}
