﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using IdentityServer.DbContexts;
using IdentityServer.Helpers;
using IdentityServer.Models;
using Newtonsoft.Json;

namespace IdentityServer.Controllers
{
    public class ScopeController : ApiController
    {
        /// <summary>
        /// Returns scope details for provided scope name.
        /// </summary>
        /// <param name="scopeName"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("getscopedetails")]
        public async Task<string> GetScopeDetails(string scopeName)
        {
            try
            {
                if (Utils.IsNull(scopeName)) return null;

                var db = new IsContext();

                var sName = scopeName.Trim().ToLower();

                var scopeProf = await db.ScopeProfiles.FirstOrDefaultAsync(a => a.Name == sName);

                if (scopeProf == null) return null;

                var sItem = Utils.ScopeProfileToScopeModel(scopeProf);

                return sItem == null ? null : JsonConvert.SerializeObject(sItem);
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Returns a list of scopes.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ActionName("getscopelist")]
        public async Task<string> GetScopeList()
        {
            try
            {
                var db = new IsContext();

                var sList = await db.ScopeProfiles.OrderBy(a => a.Name).ToListAsync();

                if (sList == null) return null;

                var nList = sList.Select(Utils.ScopeProfileToScopeModel).Where(tmpScope => tmpScope != null).ToList();

                return JsonConvert.SerializeObject(nList);
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Adds a scope.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("addscope")]
        public IHttpActionResult AddScope(ScopeModel model)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest("Invalid Model");
                if (Utils.IsNull(model.Name)) return BadRequest("Missing Scope Name");

                var db = new IsContext();

                var sName = model.Name.Trim().ToLower();

                var scopeChk = db.ScopeProfiles.FirstOrDefault(a => a.Name == sName);

                //Scope Name exists
                if (scopeChk != null) return BadRequest("Name Exists");

                var nModel = Utils.ScopeModelToScopeProfile(model);

                if (nModel == null) return BadRequest("Invalid Model");

                db.ScopeProfiles.Add(nModel);
                db.SaveChanges();

                return Ok();
            }
            catch
            {
                return BadRequest("Internal Server Error");
            }
        }

        /// <summary>
        /// Updates a scope details.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("updatescope")]
        public IHttpActionResult UpdateScope(ScopeModel model)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest("Invalid Model");
                if (Utils.IsNull(model.Name)) return BadRequest("Missing Scope Name");

                var db = new IsContext();

                var sName = model.Name.Trim().ToLower();

                var sItem = db.ScopeProfiles.FirstOrDefault(a => a.Name == sName);

                //Scope Name does not exist
                if (sItem == null) return BadRequest("Unable To Locate Scope Name");

                sItem.DisplayName = model.DisplayName;
                sItem.Type = model.Type;
                sItem.Claims = Utils.ScopeClaimListToString(model.Claims);
                sItem.ClaimsRule = model.ClaimsRule;
                sItem.ScopeSecrets = Utils.SecretListToString(model.ScopeSecrets);
                sItem.Description = model.Description;
                sItem.IncludeAllClaimsForUser = model.IncludeAllClaimsForUser;
                sItem.Emphasize = model.Emphasize;
                sItem.ShowInDiscoveryDocument = model.ShowInDiscoveryDocument;
                sItem.Required = model.Required;
                sItem.AllowUnrestrictedIntrospection = model.AllowUnrestrictedIntrospection;

                db.SaveChanges();

                return Ok();
            }
            catch
            {
                return BadRequest("Internal Server Error");
            }
        }

        /// <summary>
        /// Enables a scope.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("enablescope")]
        public IHttpActionResult EnableScope(ScopeModel model)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest("Invalid Model");
                if (Utils.IsNull(model.Name)) return BadRequest("Missing Scope Name");

                var db = new IsContext();

                var uName = model.Name.Trim().ToLower();

                var scopeProf = db.ScopeProfiles.FirstOrDefault(a => a.Name == uName);

                if (scopeProf == null) return BadRequest("Unable To Locate Scope Name");

                if (scopeProf.Enabled) return Ok();

                scopeProf.Enabled = true;

                db.SaveChanges();

                return Ok();
            }
            catch
            {
                return BadRequest("Internal Server Error");
            }
        }

        /// <summary>
        /// Disables a scope.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("disablescope")]
        public IHttpActionResult DisableScope(ScopeModel model)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest("Invalid Model");
                if (Utils.IsNull(model.Name)) return BadRequest("Missing Scope Name");

                var db = new IsContext();

                var uName = model.Name.Trim().ToLower();

                var scopeProf = db.ScopeProfiles.FirstOrDefault(a => a.Name == uName);

                if (scopeProf == null) return BadRequest("Unable To Locate Scope Name");

                if (!scopeProf.Enabled) return Ok();

                scopeProf.Enabled = false;

                db.SaveChanges();

                return Ok();
            }
            catch
            {
                return BadRequest("Internal Server Error");
            }
        }

        /// <summary>
        /// Deletes a scope.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("deletescope")]
        public IHttpActionResult DeleteScope(ScopeModel model)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest("Invalid Model");
                if (Utils.IsNull(model.Name)) return BadRequest("Missing Scope Name");

                var db = new IsContext();

                var sName = model.Name.Trim().ToLower();

                var sItem = db.ScopeProfiles.FirstOrDefault(a => a.Name == sName);

                //Scope Name does not exist
                if (sItem == null) return BadRequest("Unable To Locate Scope Name");

                db.ScopeProfiles.Remove(sItem);
                db.SaveChanges();

                return Ok();
            }
            catch
            {
                return BadRequest("Internal Server Error");
            }
        }

    }
}
