﻿using System;
using System.Linq;
using System.Threading.Tasks;
using IdentityServer3.Core.Extensions;
using IdentityServer3.Core.Models;
using IdentityServer3.Core.Services.Default;
using IdentityServer.DbContexts;
using IdentityServer.Helpers;
using IdentityServer.Models;
using IdentityServer3.Core.Services;

namespace IdentityServer.Infrastructure
{
    public class LocalUserService : UserServiceBase
    {

        public class CustomUser
        {
            public int Id { get; set; }
            public bool Enabled { get; set; }
            public string Username { get; set; }
            public string Password { get; set; }
            public string Subject { get; set; }
            public string Claims { get; set; }
            public string ProviderId { get; set; }
            public string Provider { get; set; }
        }

        private readonly IsContext _db = new IsContext();

        readonly IUserService _uService;

        public LocalUserService(IUserService uService)
        {
            if (uService == null) throw new ArgumentNullException("uService");

            _uService = uService;
        }

        public override Task AuthenticateLocalAsync(LocalAuthenticationContext context)
        {
            var users = _db.Database.SqlQuery<CustomUser>("Select * from is_Users").ToList();

            var user = users.SingleOrDefault(x => x.Username == context.UserName && Utils.Decrypt(x.Password) == context.Password);

            if (user == null) return Task.FromResult(0);

            var uClaims = Utils.StringToClaims(user.Claims);

            context.AuthenticateResult = new AuthenticateResult(user.Subject, user.Username, uClaims);

            return Task.FromResult(0);
        }

        public override Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            var users = _db.Database.SqlQuery<CustomUser>("Select * from is_Users").ToList();

            var user = users.SingleOrDefault(x => x.Subject == context.Subject.GetSubjectId());

            if (user == null) return Task.FromResult(0);

            var cList = Utils.StringToClaims(user.Claims);

            context.IssuedClaims = cList.Where(x => context.RequestedClaimTypes.Contains(x.Type));

            return Task.FromResult(0);
        }

        public override Task SignOutAsync(SignOutContext context)
        {
            _uService.SignOutAsync(context);
            return Task.FromResult(0);           
        }

        public AuthenticateResult ValidateLocalUser(UserLoginModel model)
        {
            if (Utils.IsNull(model.UserName)) return new AuthenticateResult("Missing User Name");
            if (Utils.IsNull(model.Password)) return new AuthenticateResult("Missing Password");
            if (Utils.IsNull(model.ClientId) && Utils.IsNull(model.Scope)) return new AuthenticateResult("Client And/Or Scope Is Required");

            var users = _db.Database.SqlQuery<UserProfile>("Select * from is_Users").ToList();

            var userProf =
                users.SingleOrDefault(x => x.Username == model.UserName && Utils.Decrypt(x.Password) == model.Password);

            if (userProf == null) return new AuthenticateResult("Username/Password Incorrect");

            if (Utils.IsNotNull(model.ClientId) && Utils.IsNotNull(model.Scope))
            {
                if (Utils.HasClient(model.ClientId, userProf.Claims) && Utils.HasScope(model.Scope, userProf.Claims))
                    return new AuthenticateResult(userProf.Subject, userProf.Username);
            }

            if (Utils.IsNotNull(model.ClientId) && Utils.IsNull(model.Scope))
            {
                if (Utils.HasClient(model.ClientId, userProf.Claims))
                    return new AuthenticateResult(userProf.Subject, userProf.Username);
            }

            if (Utils.IsNull(model.ClientId) && Utils.IsNotNull(model.Scope))
            {
                if (Utils.HasScope(model.Scope, userProf.Claims))
                    return new AuthenticateResult(userProf.Subject, userProf.Username);
            }

            return new AuthenticateResult("Not Authorized");
        }
    }
}