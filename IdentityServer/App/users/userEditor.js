define(["plugins/dialog", "plugins/http", "knockout", "moment", "identityServer"], function (dialog, http, ko, moment, identityServer) {

    var UserEditor = function (user) {

        this.width = "450px";
        this.bodyHeight = "auto";

        this.genders = ["", "Male", "Female"];

        this.message = ko.observable("");
        this.currentView = ko.observable("info");

        var message = this.message;
        this.message.subscribe(function () {
            setTimeout(function () {
                message("");
            }, 5000);
        })

        if (!user) user = { Claims: { Clients: [], Scopes: [] } };

        this.isEnabled = ko.observable(!!user.IsEnabled);
        this.isNewUser = !user.UserName;

        this.userName = ko.observable(user.UserName || "");
        this.password = ko.observable(user.Password || "");
        this.firstName = ko.observable(user.Claims.FirstName || "");
        this.middleName = ko.observable(user.Claims.MiddleName || "");
        this.lastName = ko.observable(user.Claims.LastName || "");
        this.nickName = ko.observable(user.Claims.NickName || "");
        this.address = ko.observable(user.Claims.Address || "");
        this.birthDate = ko.observable(moment(user.Claims.BirthDate || "").toDate());
        this.gender = ko.observable(user.Claims.Gender || "");
        this.email = ko.observable(user.Claims.Email || "");
        this.emailVerified = ko.observable(!!user.Claims.EmailVerified);
        this.phoneNumber = ko.observable(user.Claims.PhoneNumber || "");
        this.photoUrl = ko.observable(user.Claims.PhotoUrl || "");
        this.expiration = ko.observable(moment(user.Claims.Expiration || "").toDate());

        /* Update Password View */

        this.currentPassword = ko.observable("");
        this.newPassword = ko.observable("");
        this.newPasswordConfirm = ko.observable("");

        /* Clients */

        this.selectedClients = new kendo.data.ObservableArray(user.Claims.Clients);
        this.clients = new kendo.data.DataSource({
            transport: {
                read: function (options) {
                    identityServer.get("client/getclientlist", "json", function (result) {
                        options.success(JSON.parse(result));
                    });
                }
            }
        });

        /* Scopes */

        this.selectedScopes = new kendo.data.ObservableArray(user.Claims.Scopes);
        this.scopes = new kendo.data.DataSource({
            transport: {
                read: function (options) {
                    identityServer.get("scope/getscopelist", "json", function (result) {
                        options.success(JSON.parse(result));
                    });
                }
            }
        });
    };

    UserEditor.prototype.getUserModel = function () {

        var birthDate = moment(this.birthDate()).isValid()
            ? moment(this.birthDate()).format() : undefined;

        var expiration = moment(this.expiration()).isValid()
            ? moment(this.expiration()).format() : undefined;

        return {
            UserName: this.userName(),
            Password: this.password(),
            Claims: {
                FirstName: this.firstName(),
                MiddleName: this.middleName(),
                LastName: this.lastName(),
                NickName: this.nickName(),
                Address: this.address(),
                BirthDate: birthDate,
                Gender: this.gender(),
                Email: this.email(),
                PhoneNumber: this.phoneNumber(),
                PhotoUrl: this.photoUrl(),
                Expiration: expiration,
                Clients: this.selectedClients.slice(),
                Scopes: this.selectedScopes.slice()
            },
            IsEnabled: true 
        };
    };

    UserEditor.prototype.showResponseMessage = function (response) {
        var errorMessage = JSON.parse(response.responseText || "{}").Message;
        this.message(errorMessage);
    };

    UserEditor.prototype.close = function () {
        if (this.stateChanged)
            $("#users-grid").get(0).kendoBindingTarget.target.dataSource.read();

        dialog.close(this);
    };

    UserEditor.prototype.saveUser = function () {
        var modal = this;

        var method = this.isNewUser ? "user/adduser" : "user/updateuser";

        identityServer.post(method, "text", this.getUserModel(), function (result) {
            modal.stateChanged = true;
            modal.close();
        }, this.showResponseMessage.bind(this));
    };

    UserEditor.prototype.deleteUser = function () {

        var modal = this;

        dialog.showMessage("Are you sure?", "Confirmation", ["Yes", "No"])
            .then(function (dialogResult) {

                if (dialogResult !== "Yes") return;

                identityServer.post("user/deleteuser", "text", modal.getUserModel(), function (result) {
                    modal.stateChanged = true;
                    modal.close();
                }, modal.showResponseMessage.bind(modal));
            });
    };

    UserEditor.prototype.enableUser = function () {
        var modal = this;

        identityServer.post("user/enableuser", "text", this.getUserModel(), function (result) {
            modal.stateChanged = true;
            modal.isEnabled(true);
        }, this.showResponseMessage.bind(this));
    };

    UserEditor.prototype.disableUser = function () {
        var modal = this;

        identityServer.post("user/disableuser", "text", this.getUserModel(), function (result) {
            modal.stateChanged = true;
            modal.isEnabled(false);
        }, this.showResponseMessage.bind(this));
    };

    UserEditor.prototype.updatePassword = function () {
        var modal = this;

        identityServer.post("user/changeuserpassword", "text", {
            UserName: this.userName,
            OldPassword: this.currentPassword,
            NewPassword: this.newPassword,
            ConfirmNewPassword: this.newPasswordConfirm
        }, function (result) {
            modal.stateChanged = true;
            modal.message("The password has been changed");
        }, this.showResponseMessage.bind(this));
    };

    UserEditor.new = function () {
        return dialog.show(new UserEditor());
    };

    UserEditor.edit = function (user) {
        return dialog.show(new UserEditor(user));
    };

    return UserEditor;
});