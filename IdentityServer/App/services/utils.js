﻿
define(['moment'], function (moment) {

    function convertDate(value) {

        var fDate = "";
        var shortFormats = ["M-D", "M/D", "MMDD"];
        var longFormats = ["MMDDYY", "MMDDYYYY", "M-D-YY", "MM-DD-YY", "M/D/YY", "MM/DD/YY",
                           "M-D-YYYY", "MM-DD-YYYY", "M/D/YYYY", "MM/DD/YYYY"];

        if (moment(value, shortFormats, true).isValid()) {
            
            var cYear = new Date().getFullYear();
            var eMonth = moment(value, shortFormats, true).format('M');
            var eDay = moment(value, shortFormats, true).format('D');

            var nDate = new Date(cYear, eMonth - 1, eDay, 0, 0, 0, 0);

            return moment(nDate).format("MM/DD/YYYY");

        }

        if (moment(value, longFormats, true).isValid()) {

            var cYear = moment(value, longFormats, true).format('YYYY');
            var eMonth = moment(value, longFormats, true).format('M');
            var eDay = moment(value, longFormats, true).format('D');

            var nDate = new Date(cYear, eMonth - 1, eDay, 0, 0, 0, 0);

            return moment(nDate).format("MM/DD/YYYY");
        }

       
        return "";
       

        //switch (value.length) {
        //    case "3": //1-2, 1/2

        //        if (value.indexOf("-") != -1) {
        //            var tmpItems = value.split('-');
        //        }
        //        if (value.indexOf("/") != -1) {
        //            var tmpItems = value.split('/');
        //        }

        //        break;
        //    case "4": //0123, 1-23, 1/23
        //        break;
        //    case "5": //01-23, 01/23
        //        break;
        //    case "6": //012315
        //        break;
        //    case "8": //01232015, 01-23-15, 01/23/15
        //        break;
        //    case "10": //01-23-2015, 01/23/2015
        //        break;
        //}

    };

    function parseJson(jsonString) {
        try {

            var o = JSON.parse(jsonString);

            if (o && typeof o === "object" && o !== null) {
                return o;
            }
        }
        catch (e) { }

        return false;
    };

    return { convertDate: convertDate, parseJson: parseJson };

});