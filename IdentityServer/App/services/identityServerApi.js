﻿define(function () {
    return {
        settings: {
        },
        post: function (method, datatype, data, success, error) {
            $.ajax({
                type: "POST",
                url: "api/" + method,
                dataType: datatype || "text",
                data: data,
                success: success,
                error: error
            });

        },
        get: function (method, datatype, success, error) {
            $.ajax({
                type: "GET",
                url: "api/" + method,
                dataType: datatype || "text",
                success: success,
                error: error
            });
        }
    }
});