﻿define(["durandal/system", 'services/routeconfig', 'services/logger', 'services/appsecurity'],
    function (system, routeConfig, logger, appsecurity) {
        "use strict";

        var userSettings = function (username, changePassword, roles, securityHeaders) {
            var self = this;
            self.username = username;
            self.changePassword = false;
            self.roles = ko.observable(roles);
            self.loggedin = false;
        }

        return userSettings;

    });