﻿define(function (require) {
    var router = require('plugins/router'),
        appSecurity = require('services/appsecurity');

    return {
        router: router,
        appSecurity: appSecurity,
        activate: function () {
            router.map([
                { route: ['', 'users'], title: 'Users', moduleId: 'users/users', faClass: 'fa fa-users', nav: true }
            /*, { route: 'clients', title: 'Clients', moduleId: 'clients/clients', faClass: 'fa fa-suitcase', nav: true }
              , { route: 'scopes', title: 'Scopes', moduleId: 'scopes/scopes', faClass: 'fa fa-key', nav: true }*/
            ])
            .buildNavigationModel();

            return router.activate();
        }
    };
});