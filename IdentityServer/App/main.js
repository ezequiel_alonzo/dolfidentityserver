﻿requirejs.config({
    paths: {
        'text': '../lib/require/text',
        'durandal': '../lib/durandal/js',
        'plugins': '../lib/durandal/js/plugins',
        'transitions': '../lib/durandal/js/transitions',
        'knockout': '../lib/knockout/knockout-3.1.0',
        'jquery': '../lib/jquery/jquery-1.9.1',
        'moment': '../Scripts/moment.min',
        'identityServer': 'services/identityServerApi'
         
    }
});

define(function (require) {
    var system = require('durandal/system'),
        app = require('durandal/app'),
        binder = require('durandal/binder'),
        identityServer = require('identityServer'),
        appSecurity = require('services/appsecurity');
    
   // system.debug(true);

    app.title = 'Identity Server';

    app.configurePlugins({
        router: true,
        dialog: true
    });

    app.start().then(function () {
        kendo.ns = "kendo-";

        binder.binding = function (obj, view) {
            kendo.bind(view, obj.viewModel || obj);
        };
    
        if (appSecurity.isAuthenticated('Authenticated')) {
            app.setRoot('shell', 'entrance');
        }
        else {
            app.setRoot('account/login/index');
        }
    
    // set up http calls here!!
    identityServer.settings = {};
    });
});